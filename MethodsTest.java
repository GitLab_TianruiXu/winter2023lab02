
public class MethodsTest {

	public static void main(String[] args) {
		int x = 5;
		System.out.println(x);
		methodNoInputNoReturn();
		System.out.println(x);
		methodOneInputNoReturn(x + 10);
		methodTwoInputNoReturn(3, 1.5);
		int z =  methodNoInputReturnInt();
		System.out.println(z);
		double a = sumSquareRoot(9, 5);
		System.out.println(a);
		String s1 = "java";
		String s2 = "programming";
		System.out.println(s1.length());
		System.out.println(s2.length());
		System.out.println(SecondClass.addOne(50));
		SecondClass sc = new SecondClass();
		System.out.println(sc.addTwo(50));
	}
	
	
	public static void methodNoInputNoReturn() {
		System.out.println("I'm in a method that takes no input and returns nothing");
		int x = 20;
		System.out.println(x);
	}
	
	public static void methodOneInputNoReturn(int input) {
		System.out.println("Inside the method one input no return");
		input -= 5;
		System.out.println(input);
	}
	
	public static void methodTwoInputNoReturn(int input, double input2) {
		System.out.println(input);
		System.out.println(input2);
	}
	
	public static int methodNoInputReturnInt() {
		return 5;
	}
	
	public static double sumSquareRoot(int input1, int input2) {
		int sum = input1 + input2;
		return Math.sqrt(sum);
	}
	
	
}

