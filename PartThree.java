import java.util.Scanner;
public class PartThree {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		double num1 = sc.nextDouble();
		double num2 = sc.nextDouble();
		System.out.println(Calculator.add(num1, num2));
		System.out.println(Calculator.subtract(num1, num2));
		Calculator c = new Calculator();
		System.out.println(c.multiply(num1, num2));
		System.out.println(c.divide(num1, num2));		
	}
}